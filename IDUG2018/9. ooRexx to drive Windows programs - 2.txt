Another large example, this one talks to lotus notes and updates excel.  
This is a cut-down version, a number of paragraphs have been removed so you might get some errors if you tried to run it :)  

Left a lot of code in place because the routine does a lot of things you might find useful  
- reads/updates lotus notes  
- uses oorexx classes as well as our own tailored internal class  
- reads/updates/saves excel spreadsheets  
- works with ms-word  
- writes to a plain text file  
- the classes need to be in the path

Lots of fun stuff!!!











/* this routine scans the email inbox for auto-email messages from the change system. Once found */
/* it strips the necessary details and uses them to update the spreadsheet of work in progress.  */
/* When all emails have been processed the routine copies all the processed emails to client email folders  */
/* before removing those emails from the inbox.                                                             */

/* This process can run in read-only mode, which means it will do eaverything but won't change the email 	*/
/* inbox and won't auto-save the updated spreadsheet or cull old backups.									*/
/* Update mode is set via the preferences.																	*/

Start:
    pref = .pref_file~new  														/* get the preferences */
    if \pref~Valid then do
  	  j = errorDialog('Preference file unavailable. The application cannot',
	                  'continue without a valid preferences file.')
	  exit
    end

    call initialisation
    call scan_emails_for_notifications
    call process_emails_into_spreadsheet
    if pref~update_mode = 'Y' then do
	call cleanup_the_emails
	call Backup_and_cull_processing
	call save_the_spreadsheet
end
call produce_and_show_audit_report
exit






initialisation:
	/* general variables and literals used through the pgm */
  	run_start_dtime 	= .DateTime~new           					-- dtime format 2011-06-17T18:17:05.812000
	Spd_sheet		= pref~Access_Folder||pref~Spd_sht
	Spd_sht_bkup_folda	= pref~Access_Folder||pref~Spd_sht_bkup_folda||'\'
	retention_period	= 28										-- how many days to keep backups for
	notification_1 		= "CHG Notification/Australia/Contr/ORGAU"
	notification_2 		= "CHG Notification/OU=Australia/OU=Contr/O=ORGAU"
	notes_session  		= "Notes.NotesSession"
	notes_server   		= "server1/99/M/AU"
	notes_Teamfile		= "mail1\TEAM1.NSF"
	notes_vw	   	= "$inbox"
	notes_dest_fldr 	= "AA Changes/Problems\"
	notes_other		= "Other"
	new_email_mark 		= "************ANOTHER DBA EMAIL!!************"
	nil_array 		= .array~of(.nil)
	xcel_updated   		= .false
	xcel_open 	   	= .false

	/* spreadsheet columns */
	col_name 			= 'A'
	col_PCT  			= 'B'
	col_desc 			= 'C'
	col_clnt 			= 'D'
	col_tikt 			= 'E'
	col_sev  			= 'F'
	col_tech 			= 'G'
	col_strt 			= 'H'
	col_end  			= 'I'
	col_comp 			= 'J'

	/* spreadsheet settings */
	on_hold				= 'H'
	completed			= 'Y'
	rejected			= 'R'
	transferred			= 'T'
	chg_cancelled		= 'C'
	chg_rejected		= 'R'
	DHA_oracl_tech  	= 'ORACLE'
	DHA_sparc_tech  	= 'OBJSTR'
	xl_black 			= 1
	xl_red	 			= 3
	xl_green 			= 4
	xl_blue  			= 5
	xl_yellow			= 6
	xl_pink	 			= 7
	st_open				= 'Open'
	st_assign			= 'ASSIGN'
	st_sevchg			= 'Sev change'
	st_resolved			= 'Resolved'
	st_restored			= 'Restored'
	st_transfer			= 'TRANSFER'
	no_descr			= '**** NO DESCRIPTION PROVIDED ****'
	oth_team_descr 		= 'APPROVE OTHER TEAM CHANGE'


	/* stem collection for storing email lines and the run audit info */
  	emails_read 		= 0
  	emails_used 		= 0
  	emails_ignored 		= 0
  	emails_copied 		= 0
  	emails_deleted		= 0
	line	 			= .stem~new('line.')
	report	 			= .stem~new('report.')
	line_ix  			= 0
	rpt_ix 	 			= 0

	/* get the lotus notes session up and running */
	lnotesSess			= .oleobject~new(notes_session)
	lnotesdb  			= lnotesSess~GetDatabase(notes_server,notes_Teamfile)
	lnotesvw 			= lnotesdb~Getview(notes_vw)

	/* notify users of the run starting */
  	if pref~update_mode = 'Y' then
		say 'Running in UPDATE mode. Emails and spreadsheet changes will be saved'
  	else
  		say 'Running in READ-ONLY mode. Emails will be unchanged and spreadsheet will not be saved'
  	say 'Run started at 'run_start_dtime

return







Backup_and_cull_processing:
	/* The backup is taken AFTER the automated changes are made to the spreadsheet, because by now the emails used for	*/
	/* the updates have been moved and deleted, so a rerun is hard to set up, so lets protect what has just been done. 	*/
	temp = pref~Spd_sht
	parse var temp docname '.' suffix
	bkupdt = (run_start_dtime~standardDate'-'run_start_dtime~normaltime)~changeStr(':','')
	bkup = Spd_sht_bkup_folda||docname||' '||bkupdt||'.'||suffix
	rc=SysFileCopy(Spd_sheet, bkup)
	if rc \= 0 then do
		call add_to_report 'Spreadsheet backup failed with rc 'rc':'SysGetErrorText(rc)
		call add_to_report 'Spreadsheet should be backed up manually'
		return
	end

	/* Determine a bkup removal date of 4 weeks (28 days) ago	 											*/
	/* We will only get rid of our own backups - NOT everything in the folder that exceeds the date limit 	*/
	/* backup names should have format of "chgmails 20110708-104159.xls" 									*/
	retention_date = .DateTime~new~addDays(retention_period*-1)~standardDate||'-999999'	-- subtract by adding -ve days, and set time to maximum

	/* search the directory for all "chgmails*.xls" files, results to a stem "files", options are 	*/
	/*		F - Search only for files																	*/
	/*		S - Search subdirectories recursively														*/
	/* 		L - return Return the time and date in the form YYYY-MM-DD HH:MM:SS							*/
	rc = sysfiletree(Spd_sht_bkup_folda||docname||'*'||'.'||suffix,'files','FSL')
	if rc \= 0 then do
		call add_to_report 'Fatal error getting backup file list with rc 'rc':'SysGetErrorText(rc)
		call add_to_report 'Cull of old backups not completed'
		return
	end

	/* now work through the files and delete those that are too old 										*/
	do i = 1 to files.0
		parse var files.i moddate modtime filesize fileattr fullname	/* get full file info  				*/
		docname = filespec('name',fullname)            					/* get the filename        			*/
		parse var docname doctitle docdate '.' docsuffix				/* get the file backup date-time 	*/
		if docdate < retention_date then do
			rc = sysfiledelete(fullname~strip)
			if rc \= 0 then do
				call add_to_report 'Fatal error deleting old backup file "'fullname'"'
				call add_to_report '  with rc 'rc':'SysGetErrorText(rc)
				call add_to_report 'Cull process aborted'
				return
			end
			call add_to_report 'Backup file "'docname'" deleted'
		end
	end
Return










scan_emails_for_notifications:
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
	/*  Work backwards thru team inbox extracting info from ALL emails */
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
	lnotesDoc 	= lnotesvw~GetLastDocument
	DO while lnotesDoc <> .nil
	  from = lnotesDoc~GetFirstItem("From")~Text
	  If from = notification_1 | from = notification_2 then do
		  /* IMPORTANT - it can take some time to read through the inbox, and other emails can	*/
		  /* arrive after we've started. This test checks whether this has happened. Email time */
		  /* does not have microseconds. To avoid any issues from this, no equality is in the   */
		  /* test. Only emails < the start time are processed. Any ignored will simply get      */
		  /* picked up on the next run. It is critical that this is the same test that is used 	*/
		  /* at the end to determine whether to move an email from the inbox. This should ensure*/
		  /* that none fall through the cracks.													*/
		  date_recd = lnotesDoc~Created
		  parse var date_recd dd '/' mm '/' yyyy hh ':' mi ':' ss ampm
		  if ampm = 'PM' & hh <> 12 then hh = hh + 12
		  email_dtime = .Datetime~new(yyyy,mm,dd,hh,mi,ss)
		  if email_dtime < run_start_dtime then do
		  	call read_email_body
		  	emails_used = emails_used + 1
		  end
	  END
	  emails_read = emails_read + 1
	  lnotesDoc = lnotesvw~GetPrevDocument(lnotesDoc)
	END

	If line~items = 0 then DO
		j = errorDialog('No Problem or Change Emails found. No spreadsheet updates done. Scan process completed.')
		call  produce_and_show_audit_report
		exit
	END
return







read_email_body:
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	  This process will break the 'body' of the notes document into readable lines of output
	  for extraction into the spreadsheet. Results will be in line1, line2, line3 etc
	  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

  	  subject 	= lnotesDoc~GetFirstItem("Subject")~Text
  	  body 		= lnotesDoc~GetFirstItem("Body")~Text

	/*  Determine the line break byte (usually x'0D') by stripping the known structures of the first line */
	/* If the first line is different than what we want/need, just jump out without doing anything which  */
	/* will push us to the next email document 															  */
	line_break = ''
	Select
		When word(body,1) = 'Org:' 											then line_break = substr(word(body,2),4,1)
		When word(body,1) = 'Approval' 										then line_break = substr(word(body,6),10,1)
		When word(body,1) = 'Change' & pos('completed.',word(body,5)) > 0 	then line_break = substr(word(body,5),11,1)
		When word(body,1) = 'Change' & pos('approved.',word(body,5)) > 0 	then line_break = substr(word(body,5),10,1)
		When word(body,1) = 'Change' & pos('rejected.',word(body,5)) > 0 	then line_break = substr(word(body,5),10,1)
		When word(body,1) = 'Change' & pos('Rejected',word(body,8)) > 0 	then line_break = substr(word(body,8),9,1)
		When word(body,1) = 'Change' & pos('assigned',word(body,5)) > 0 	then line_break = substr(word(body,7),5,1)
		When word(body,1) = 'Change' & pos('Held',word(body,8)) > 0 		then line_break = substr(word(body,8),5,1)
		When word(body,1) = 'Change' & pos('Saved',word(body,8)) > 0 		then line_break = substr(word(body,8),6,1)
		When word(body,1) = 'Change' & pos('completed.',word(body,5)) = 0 	then line_break = substr(word(body,9),10,1)
		Otherwise do
			call add_to_report 'Ignored - subject "'subject'"'
			emails_ignored = emails_ignored + 1
			return
			end
	END
	/* most often a carriage-return will be followed by a line-feed (x'0A'). If thats the case here then lump them together */
	if C2x(line_break) = '0D' then do
		next_byte = substr(body,(pos(line_break,body)+1),1)
		if C2x(next_byte) = '0A' then line_break = line_break||next_byte
		end
	else do
		say "Couldn't determine the line break! That's going to cause a program crash."
		say "Line (ch 1-80) in question is"
		say "|------------------------------------------------------------------------"
		say substr(body,1,80)
		say "|------------------------------------------------------------------------"
		say "Run stopped."
		say "Remove the email(s) and run again or modify the code to handle the email content"
		exit
	end

	/* Now that we know what our line break is, we can use it to break the email body into lines */
	still_to_do = body
	first_doc_line = .true
	do forever
		parse var still_to_do new_line (line_break) still_to_do
		if still_to_do = '' then leave
		if new_line \= '' then do											/* can get a blank line if we get 2 CR-LF in a row */
			line_ix = line_ix + 1
			if first_doc_line then do										/* mark the start of the next email and write the subject */
				line[line_ix]	= new_email_mark
				line_ix 	 	= line_ix + 1
				line[line_ix]	= 'Subject: 'subject
				line_ix 	 	= line_ix + 1
				line[line_ix]	= new_line||' date received: 'date_recd		/* may be used later to set the end date */
				first_doc_line 	= .false
				end
			else
				line[line_ix]	= new_line
		end
	end
return









process_emails_into_spreadsheet:
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
	/*  Read all lines and extract info to load into spreadsheet */
	/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

	/*  this section starts on a subject line and scans forward, branches to a chg or prob section as needed
		to keep scanning and on return we'll have gone through the remaining lines for this chg/pblm */
	Do i = 1 to line_ix
		call initialise_spreadsheet_vars
		if pos('Subject: ',line[i]) > 0 then do
			if word(line[i],2) = 'Change' | pos('approval Chg ',line[i]) > 0 then
				 call scan_chg
			else call scan_PR
		end
	END
return







initialise_spreadsheet_vars:
	mn_num      	= ''
	MN_type     	= ''
	assignee    	= ''
	PR_num      	= ''
	chgnum      	= ''
	sched_dt_dmy	= ''
	sched_dt    	= ''
	descr	     	= ''
	sev         	= ''
	approva     	= ''
	actioner    	= ''
	fin_date    	= ''
	chg_code    	= ''
	prstatus    	= ''
	Paste_reqd  	= ''
	closed_by_us	= ''
	work_status	= ''
return








scan_chg:
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
/*   A change has been tracked, so setup variables for spreadsheet      */
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

 	MN_Type 	= 'C'
 	this_email_done = .false
 	scan_to_end 	= .false

	do while i <= line_ix & \this_email_done		-- i is the line index from the parent routine
		if approva = '' then do
			if pos('has been approved.',line[i]) > 0 |,
			   pos('has been rejected',line[i]) > 0  |,
			   pos('has been assigned',line[i]) > 0  |,
			   pos('has been created',line[i]) > 0 		then approva = word(line[i+2],3)
			if pos('has been completed',line[i]) > 0 	then approva = word(line[i+3],3)
			if pos('Submitted from Held',line[i]) > 0 	then approva = word(line[i+2],3)
			if pos('Submitted from Saved',line[i]) > 0 	then approva = word(line[i+2],3)
		end
		Select
			When pos(new_email_mark,line[i]) > 0 then this_email_done = .true			/* we've run into another email */
			When scan_to_end 			then nop
			When line[i] = '' 			then nop
			When pos('Approval by group',line[i]) > 0 then approva = word(line[i],4)	/* change approver */
			When pos('Change #:',line[i]) > 0 then chgnum = word(line[i],3)				/* extract change number */
			When pos('Assigned Group:',line[i]) > 0 then DO    							/* Assignee Group   */
			  	assignee = word(line[i],3)
			  	If pos("_",approva) > 0 & assignee <> approva then descr = oth_team_descr /* approve another team chg */
				End
			When pos('has been completed',line[i]) > 0 then DO    						/* Change complete  */
				find_date = wordpos('received:',line[i])								/* todays date added by this pgm */
			  	fin_date = right(word(line[i],find_date + 1),10,0)
				End
			When pos('has been rejected',line[i]) > 0 then DO    						/* Change complete/rejected  */
			  	chg_code = chg_rejected
			  	find_date = wordpos('received:',line[i])								/* todays date added by this pgm */
				fin_date = right(word(line[i],find_date + 1),10,0)
				End
			When pos('Completion Code:',line[i]) > 0 & ,
				 pos('CANCELLED',line[i]) > 0 then DO  									/* Change complete/cancelled  */
			  	chg_code = chg_cancelled
  			  	fin_date = .team_date~new~team_dt_format								/* use todays date */
				End
			When pos('Assigned User:',line[i]) > 0 then DO
			  	If pos('_',line[i]) > 0 | pos('unassigned',line[i]) > 0 |,
			       pos('Wicks',line[i]) > 0 							|,
			       descr = oth_team_descr					 			|,
			       pos('unknown',line[i]) > 0 | substr(word(line[i],3),1,2) = 'AU' then nop
			  	Else DO
	      		   actioner = subword(line[i],3)
	   			   If pos(',',actioner) > 0 then do 									/* comma means surname is first */
					  parse var actioner surname ',' c_name
					  actioner = c_name~strip' 'surname~strip
				   end
		        end
				End
			When pos('Scheduled date/time:',line[i]) > 0 then
				sched_dt = .team_date~fromStandardDate(changestr('-',word(line[i],3),''))	-- Turn word(line[i],3) yyyy-mm-dd into a date object
		  	When pos('Change abstract:',line[i]) > 0 then DO
		    	If descr = '' then descr = subword(line[i],3)			/* only set the desc if chg is not approval for another team */
				scan_to_end = .true										/* 'abstract' is last line we need so skip to the next email */
		    	End
		  	Otherwise nop
		End
		if \this_email_done then i = i + 1
	End
	sched_dt_dmy = sched_dt~team_dt_format		/* convert to dd/mm/yyyy  */
   	Call Update_Spreadsheet
Return









Update_Spreadsheet:
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/
/*  Use information extracted from the email to update spreadsheet      */
/*@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@*/

	/*  define Excel object & make sure activity can be seen.  */
	xcel_updated = .true
	If \xcel_open then DO
	    clipboard			=.WindowsClipboard~new
	    getExisting			=.true						-- try to use an existing instance of the app
		xcel=.OleObject~New('excel.Application',,getExisting)
		xcel~Visible		= .true
		xcel~Workbooks~open(Spd_sheet)
		xcel~Sheets('Problem Change')~Select
		xcel~displayalerts 	= .false	 				-- Don't warn for macros, if file already exists, etc.
		xcel~Visible		= .true
		xlLastCell			= xcel~GetConstant('xlLastCell')
		xlUp				= xcel~GetConstant('xlUp')
		xlCellValue			= xcel~GetConstant('xlCellValue')
		xlExpression		= xcel~GetConstant('xlExpression')
		xlEqual				= xcel~GetConstant('xlEqual')
		xlNotEqual			= xcel~GetConstant('xlNotEqual')
		xlLessEqual			= xcel~GetConstant('xlLessEqual')
		xlLeft				= xcel~GetConstant('xlLeft')
		xlRight				= xcel~GetConstant('xlRight')
		xlCenter			= xcel~GetConstant('xlCenter')
		xlTrue				= .true
		xcel_open= .true
	End

	--  Let's check to make sure that this change/problem number is not already listed
	--  First find the last active row in the 'ticket' column (note: xlLastCell isn't really the last cell)
	lastcell = xcel~ActiveCell~SpecialCells(xlLastCell)~Address
	parse var lastcell '$'max_column'$'max_row
	last_row = xcel~cells(max_row,col_tikt)~End(xlUp)~Row

    /* now load the entire 'ticket' column into an empty clipboard and thence to an array so its easier to work with */
	clipboard~Empty
    xcel~Range(col_tikt||'1:'col_tikt||last_row)~Unknown('Copy',nil_array)
    all_nbrs=clipboard~makearray

    /* now we can scan the column to see if our nbr is already in the sheet    		*/
	If MN_type = 'P' then mn_num = PR_num   	/* the nbr for a Problem record ?  	*/
	If MN_type = 'C' then mn_num = chgnum   	/* the nbr for a change record ?   	*/
	nbr_found = all_nbrs~hasItem(mn_num)
	if nbr_found then
		call modify_a_row
	else
		call insert_a_row
return







modify_a_row:
		/* S/sheet nbr matches change system nbr - its already in the spreasheet so must be an update */
		this_row 	= all_nbrs~index(mn_num)							/* locate the row to be updated 			*/
		cdat 		= right(xcel~cells(this_row,col_strt)~value,10,0)	/* extract current start dt with leading '0'*/
		cact 		= xcel~cells(this_row,col_name)~value  				/* extract name of current actioner 	    */
		work_status = xcel~cells(this_row,col_comp)~value				/* extract current work status				*/
		old_sev 	= xcel~cells(this_row,col_sev)~value				/* extract current severity level			*/
		finished 	= xcel~cells(this_row,col_end)~value				/* extract current finish date				*/

		fields_updated = .false
		call check_for_field_updates
		If cdat = sched_dt_dmy | Prstatus = st_resolved then
			call check_for_completion
		Else
			if MN_type = 'C' then call check_for_schedule_chg			/* start dt only updates on change records */
			else call check_for_problem_updates
return



check_for_field_updates:
	if actioner <> '' & actioner <> cact then DO					/* assignee name has changed */
		fields_updated = .true
		xcel~cells(this_row,col_name)~value = actioner
		call add_to_report 'Assignee name changed from 'cact' to 'actioner,mn_num
	END
	If Prstatus = st_sevchg then do
		fields_updated = .true
		xcel~cells(this_row,col_sev)~value = sev
		call add_to_report 'Severity changed from 'old_sev' to 'sev,mn_num
	END
	If Prstatus = st_transfer then do								/* a tfr is like a completed pblm,	*/
		fields_updated = .true
		xcel~cells(this_row,col_end)~value = sched_dt_dmy			/* so set the end date and		 	*/
		xcel~cells(this_row,col_name)~value = actioner				/* the assignee is where we sent it	*/
	   	xcel~cells(this_row,col_comp)~value = transferred
		call add_to_report 'Problem transferred from 'cact' to 'actioner,mn_num
	end
return



check_for_completion:
	if fin_date <> '' then DO								/* Change/Problem has been completed 	*/
  			xcel~cells(this_row,col_end)~value = fin_date	/* set the end date 					*/
  			if cact = 'UNASSIGNED' then do
				call add_to_report '****** Possible closure of UNASSIGNED record',mn_num
			end
  			If work_status = on_hold |,
  			   work_status = rejected then
  			   	xcel~cells(this_row,col_comp)~value = completed
  			If chg_code <> '' then DO						/* change cancelled or rejected 		*/
				xcel~cells(this_row,col_comp)~value = chg_code
				call add_to_report 'Change marked as complete with status reset from 'work_status' to 'chg_code, mn_num
				end
			else do
				if MN_type = 'C' then call add_to_report 'Change marked as complete',mn_num
				else call add_to_report 'Problem marked as complete',mn_num
			end
  		END
  	else if \fields_updated then DO			/* if no other changes maybe its a status update? If so reset the s/sheet */
  			If work_status = rejected then DO
  				xcel~cells(this_row,col_end)~value = ''
  				xcel~cells(this_row,col_comp)~value = ''
  				xcel~cells(this_row,col_comp)~formula='=IF(I'this_row'>0,"Y"," ")'
				call add_to_report 'Change UN-rejected',mn_num
			end
		end
return





check_for_problem_updates:
	/* if this problem is being re-assigned but is marked as finished, then we have to 'unfinish' it */
	If Prstatus = st_assign &,						  		/* is this email trying to assign the problem? */
	   POS("/",finished) > 0 &,								/* is this pblm marked as finished? */
	   work_status <> st_held &,							/* is this pblm not on hold			*/
	   cact <> 'UNASSIGNED' then DO							/* was this pblm previously assigned? */
		xcel~cells(this_row,col_end)~value = ''				/* then reset the fields to 'unfinish' it*/
		xcel~cells(this_row,col_comp)~value = ''
		xcel~cells(this_row,col_comp)~formula='=IF(I'this_row'>0,"Y"," ")'
  		call add_to_report 'Problem re-opened due to re-assigning',mn_num
	END
return





check_for_schedule_chg:
	/* scheduled start date has changed for pblm or chg, AND problem status is not set to 'resolved' */
	xcel~cells(this_row,col_strt)~value = sched_dt_dmy
	call add_to_report 'Scheduled date changed from 'cdat' to 'sched_dt_dmy,mn_num

	If work_status = rejected then DO 								/* Change UN-Rejected */
		xcel~cells(this_row,col_end)~value = ''
		xcel~cells(this_row,col_comp)~value = ''
		xcel~cells(this_row,col_comp)~formula='=IF(I'this_row'>0,"Y"," ")'
  		call add_to_report 'Change UN-rejected',mn_num
	END

	/* Process the reschedule, we may have to move the row in the spreadsheet. 	*/
	ins_row = find_an_insertion_point(sched_dt)
	if this_row <> ins_row then do
		xcel~Rows(this_row':'this_row)~Unknown('Cut',nil_array)
		xcel~Rows(ins_row':'ins_row)~Insert
	END
return








insert_a_row:
	/* find the insertion point, empty the clipboard so nothing gets pasted, make sure new row has no copied formatting */
	ins_row = find_an_insertion_point(sched_dt)
	clipboard~Empty
	xcel~Rows(ins_row':'ins_row)~Insert

	/* the row is in place so go through and set field formats and conditiional formats to exactly as needed */
	xcel~range(col_name||ins_row':'col_end||ins_row)~clear
   	xcel~cells(ins_row,col_desc)~Select
	xcel~Selection~FormatConditions~Delete
	xcel~Selection~FormatConditions~Add(xlCellValue,xlEqual,oth_team_descr)				-- cond format for 'approve other team chg'
	xcel~Selection~FormatConditions~Item(1)~Font~Bold=xlTrue
	xcel~Selection~FormatConditions~Item(1)~Font~ColorIndex = xl_blue
	xcel~Selection~HorizontalAlignment=xlLeft

   	xcel~cells(ins_row,col_clnt)~Select
	formula1 = '='||col_end||ins_row||'<>""'
	xcel~Selection~FormatConditions~Add(xlExpression,,'='||col_end||ins_row||'<>""')	-- cond format set client green if work completed
	xcel~Selection~FormatConditions~Item(1)~Font~Bold=xlTrue
	xcel~Selection~FormatConditions~Item(1)~Font~ColorIndex = xl_green
	xcel~Selection~FormatConditions~Add(xlCellValue,xlNotEqual,'z')						-- otherwise set it to red
	xcel~Selection~FormatConditions~Item(2)~Font~Bold=xlTrue
	xcel~Selection~FormatConditions~Item(2)~Font~ColorIndex = xl_red

   	xcel~cells(ins_row,col_sev)~Select
	xcel~Selection~FormatConditions~Delete
	xcel~Selection~FormatConditions~Add(xlCellValue,xlLessEqual,2)						-- cond format sev 1,2 is red
	xcel~Selection~FormatConditions~Item(1)~Font~Bold=xlTrue
	xcel~Selection~FormatConditions~Item(1)~Font~ColorIndex = xl_red

	xcel~cells(ins_row,col_name)~HorizontalAlignment	= xlLeft
	xcel~cells(ins_row,col_PCT)~HorizontalAlignment		= xlLeft
	xcel~cells(ins_row,col_desc)~HorizontalAlignment	= xlLeft
	xcel~cells(ins_row,col_clnt)~HorizontalAlignment	= xlLeft
	xcel~cells(ins_row,col_tikt)~HorizontalAlignment	= xlRight
	xcel~cells(ins_row,col_sev)~HorizontalAlignment		= xlCenter
	xcel~cells(ins_row,col_tech)~HorizontalAlignment	= xlLeft
	xcel~cells(ins_row,col_strt)~HorizontalAlignment	= xlRight
	xcel~cells(ins_row,col_end)~HorizontalAlignment		= xlRight
	xcel~cells(ins_row,col_comp)~HorizontalAlignment	= xlCenter
	xcel~cells(ins_row,col_strt)~NumberFormat 			= "dd/mm/yyyy;@"
	xcel~cells(ins_row,col_end)~NumberFormat 			= "dd/mm/yyyy;@"

	/*If ((assignee <> 'DHA' & clnt <> 'DHA') & ,
	    (assignee <> 'CUA' & clnt <> 'CUA') & actioner = '') then
		call Extract_people	*/

	If descr = '' then descr = no_descr
	xcel~cells(ins_row,col_tikt)~value = mn_num
	xcel~cells(ins_row,col_PCT)~value = MN_Type
	xcel~cells(ins_row,col_desc)~value = descr
	xcel~cells(ins_row,col_comp)~formula='=IF(I'ins_row'>0,"Y"," ")'

   	/* set description font to blue and formula to 'N' if doing other team approvals, or red if no descr provided */
	xcel~cells(ins_row,col_desc)~Select
	xcel~Selection~Font~ColorIndex = xl_black
	If descr = no_descr then do
		xcel~Selection~Font~ColorIndex = xl_red
		call add_to_report 'No description provided',mn_num
	end
	If descr = oth_team_descr then DO
		xcel~Selection~Font~ColorIndex = xl_blue
		xcel~Selection~Font~Bold = .true
		xcel~cells(ins_row,col_comp)~formula = '=IF(AND(C'ins_row'="'descr'",I'ins_row'> 0),"N","")'
	END

	/*  Have found that sometimes the RESOLVED Email can be found before the OPEN PR     */
	/*  email has been read. Therefore, we need to insert the row with the closed date   */
	If fin_date <> '' then DO
		xcel~cells(ins_row,col_end)~value = fin_date
		If sched_dt_dmy = '' then sched_dt_dmy = fin_date
	END
	xcel~cells(ins_row,col_strt)~value = sched_dt_dmy

	/* things specific to problem records 												*/
	If MN_type = 'P' then DO
		xcel~cells(ins_row,col_clnt)~value = assignee
		xcel~cells(ins_row,col_sev)~value = sev
		if sev <= 2 then do
			xcel~cells(ins_row,col_sev)~Select
			xcel~Selection~Font~ColorIndex = xl_red
		end
	END

	call add_to_report 'Inserted for 'sched_dt_dmy,mn_num
Return










Check_day:
      our_yy = substr(sched_dt_dmy,9,2)
      our_mm = substr(sched_dt_dmy,4,2)
      our_dd = substr(sched_dt_dmy,1,2)
      our_date = our_yy||'/'||our_mm||'/'our_dd
      days = 'Monday Tuesday Wednesday Thursday Friday Saturday Sunday'
      tday = date('w')
      num_days = date('B',our_date,'O') - date('B') /* days from now to sched */
      If num_days < 0 then DO
        b4_tday = 'YEP'
        return
      END
      start_day = wordpos(tday,days)           /* which day of week (eg 3rd?) */
      If num_days < start_day then week = num_days + start_day
      Else DO
         num_days = num_days // 7
         week = num_days + start_day
      END
      sched_day = word(days,week)
Return








cleanup_the_emails:
	/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */
	/* If the Spreadsheet has been successfully updated, move Emails to client folders  */
	/* Check date/time of email. We don't want to delete one that hasn't been put in 	*/
    /* spreadsheet ... ie an email may have been received AFTER kicking off the script.	*/
    /* Comes in as "dd/06/2011 hh:mm:ss pm" from lotus									*/
    /* We also can't process the emails directly...as they get deleted the inbox view	*/
    /* will change. So grab a snapshot on the inbox via an alis list, and work off that.*/
	/* @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ */

  	lnotesAll = lnotesvw~AllEntries											-- create a non-updating alias list into the inbox
	lnotesEnt = lnotesAll~GetFirstEntry

	DO while lnotesEnt <> .nil
		lnotesDoc = lnotesEnt~Document										-- get the email that relates to this alias
    	from = lnotesDoc~GetFirstItem("From")~Text
		If from = notification_1 | from = notification_2 then DO
			/* We've not processed emails that arrived after the run start so leave them alone. 				*/
			/* This is the same test that is used at the top of the pgm to determine which emails to process.  	*/
		    email_dt = lnotesDoc~Created
			parse var email_dt dd '/' mm '/' yyyy hh ':' mi ':' ss ampm
			If ampm = 'PM' & hh <> 12 then hh = hh + 12
			email_dtime = .Datetime~new(yyyy,mm,dd,hh,mi,ss)
			if email_dtime < run_start_dtime then do
		    	client = ''
	      		subject = lnotesDoc~GetFirstItem("Subject")~Text
	      		Body = lnotesDoc~GetFirstItem("Body")~Text
			if wordpos(client,client_list) = 0 then
	       			lNotesDoc~PutInFolder(notes_dest_fldr||notes_other)		-- COPY to the 'unknown client' fldr
	       		else
	       			lNotesDoc~PutInFolder(notes_dest_fldr||client)			-- COPY to the client fldr
	       		emails_copied = emails_copied + 1
	    		lNotesDoc~RemoveFromFolder(notes_vw)						-- remove this email from the inbox
   				emails_deleted = emails_deleted + 1
	    	End
	    end
		lnotesEnt = lnotesAll~GetNextEntry(lnotesEnt)
	END
return






add_to_report:
arg txt_in,num_in
	rpt_ix 			  = rpt_ix + 1
	report[txt,rpt_ix]= txt_in
	report[num,rpt_ix]= num_in
return







find_an_insertion_point:
arg date_in
    /* load the 'start date' column into an array. The column should already be sorted in the spreadsheet */
	clipboard~Empty
    xcel~Range(col_strt||'1:'col_strt||last_row)~Unknown('Copy',nil_array)
    all_dates=clipboard~makearray

    /* now we can scan the dd/mm/yyyy start dates to see where to put our row  */
	do dt = last_row to 1 by -1
		row_date = all_dates[dt]
		parse var row_date dd '/' mm '/' yyyy
		row_dt = .Datetime~new(yyyy,mm,dd)
		if date_in >= row_dt then leave
	end
	insertion_row = dt + 1

return insertion_row






produce_and_show_audit_report:
	/* write the report stem content to a txt file, then open the txt file in notepad for inspection before ending the pgm */
	/* If for some reason the file can't be created, dsiplay the report to the run window for inspection.                  */
   	run_end_dtime  	= .DateTime~new
  	run_time 		= run_start_dtime - run_end_dtime
  	say 'Run ended at   'run_end_dtime
	say ' '
	say ' '

  	bkupdt = (run_start_dtime~standardDate'-'run_start_dtime~normaltime)~changeStr(':','')
	reportfile = pref~Access_Folder||'Run report '||bkupdt||'.doc'

	file=.stream~new(reportfile)								/* create new file object 			*/
    if file~open('write replace') \= 'READY:' then do			/* will create a new file if needed */
   		msg = "Problem creating run report '"file~description"'"||return||"Results written to display window"
    	j = errorDialog(msg)
    	call display_audit_report
    	return
    end

  	if pref~update_mode = 'Y' then
		file~lineout('Running in UPDATE mode. Emails and spreadsheet changes will be saved')
  	else
  		file~lineout('Running in READ-ONLY. Emails unchanged, spreadsheet will not be saved')
  	file~lineout('Run started at 'run_start_dtime)
  	file~lineout('Run ended at   'run_end_dtime)
    file~lineout(' ')
    file~lineout(' ')
    file~lineout('******* Run report *******')

	/* Since there are 2 data items per entry, 'report~items' actually reports double the nbr of entries. */
	/* So use the index. Probably would've been easier to use an array. Oh well */
	do rpt_ix = 1 to rpt_ix
		mn_num = left(report[num,rpt_ix],10," ")
		file~lineout('Nbr 'mn_num' - 'report[txt,rpt_ix])
	end

	file~lineout('  Emails read from the inbox        'emails_read)
	file~lineout('  Emails from Change system         'emails_used)
	file~lineout('  Emails not useful / ignored       'emails_ignored)
  	file~lineout('  Emails copied to client folders   'emails_copied)
  	file~lineout('  Emails deleted from the inbox     'emails_deleted)
	If xcel_updated then file~lineout('  The spreadsheet content has changed')
  	file~lineout('  Run time (h:m:s)   'run_time~hours':'run_time~minutes':'run_time~seconds)
    file~lineout('****** End of report ******')
    file~close

	/* present the audit report file via MS-Word */
	msWord 			= .OLEObject~New("Word.Application")
	msWord~Visible 	= .TRUE
	msWordDocs 		= msWord~Documents
	msWordDocs~Open(reportfile)

return





display_audit_report:
	/* this section will only get executed if there is a problem creating the run report txt file. */
	/* It simply displays the report to the run window. From there the user can close or save it.  */
	say '******* Run report *******'
	do rpt_ix = 1 to rpt_ix
		mn_num = left(report[num,rpt_ix],10," ")
		say "Nbr "mn_num" - "report[txt,rpt_ix]
	end
	say '  Emails read from the inbox        'emails_read
	say '  Emails from ManageNow             'emails_used
	say '  Emails not useful / ignored       'emails_ignored
  	say '  Emails copied to client folders   'emails_copied
  	say '  Emails deleted from the inbox     'emails_deleted
	If xcel_updated then say '  The spreadsheet content has changed'
  	say '  Run time (h:m:s)   'run_time~hours':'run_time~minutes':'run_time~seconds
	say '****** End of report ******'
return




save_the_spreadsheet:
    xcel~Workbooks(1)~close(.true)
    xcel~quit
return







::requires "OODPLAIN.CLS"
::REQUIRES "orexxole.cls"
::requires "WINSYSTM.CLS"
::requires 'oodwin32.cls'
::requires "prefs.cls"





::class team_date subclass DateTime public
/*  the Datetime class has all sorts of wonderous date formats you'll likely never ever need!...EXCEPT...the ability to return
	a date in dd/mm/yyyy format. So we're going to have to do it ourselves, and put the result into a specific attribute.
	Arguments for new instances are same as for DateTime class eg.
	  	fin_dat2 = .team_date~new                              				for todays date
	  	fin_dat2 = .team_date~new~team_dt_format               				for todays date in the team format
	  	fin_dat2 = .team_date~new(2000,02,28)   			   				for known date elements yyyy mm dd
	  	fin_dat2 = .team_date~new(2000,02,28)~team_dt_format   				to get it back in the team format
  		fin_dat2 = .team_date~fromEuropeanDate('27/02/00')~team_dt_format	for using a known date to get team format
*/

::method team_dt_format
	-- all we need here is an instance of the date object. This routine is simply to put the century back in
	newdate = self~standardDate(' ')~string			-- turn the date into a string with yyyy (to keep the leading zeroes)
	parse var newdate newyyyy newmm newdd			-- break it up, then...
	newdate = newdd||'/'||newmm||'/'||newyyyy		-- put is back together the way we want
return newdate

