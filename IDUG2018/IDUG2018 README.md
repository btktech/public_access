# README #
This folder contains REXX coding examples that go with the "2018 IDUG DB2 Australasian Tech Conference" presentation "The DB2/Rexx advantage".


### The examples are ###

**1. %execute  **  
This code is available at http://gsf-soft.com/Freeware/  
The routine is useful to provide a Rexx routine as inline-JCL without needing to use a PDS. Also, part of it's capability is to let you execute a chunk of Rexx code (or an entire program) real-time from the editor (this is where I use it most).


**2. Instream Rexx parms**  
This example shows how you can use JCL parms to parameterise your Rexx routines. This can be especially useful when combined with JCL procs.  
This can be useful when you have to execute the same DB2 commands against multiple DB2 targets. Each command would need it's own SSID, eg. for a bind.

**3. Instream JCL parms**  
This example shows how you can use Rexx with JCL parms to tailor instream JCL (which normally can't pick up on JCL variables). This can be useful when you have a number of input files that need to be parameterised in the same way, you can use Rexx parsing as a very effective translator.

**4. interpret and sourceline**  
This example shows how you can use 'interpret' and 'sourceline'. These can be hugely handy.  

* interpret - evaluates a variable as a rexx instruction. Being able to use variables when building commands can make a program infinitely flexible  
* sourceline - reads the source code for the program you're in. Can be used to tuck commands away in the code, also hugely useful to embed ISPF objects and non-Rexx text within the Rexx program (see example *Inline everything*).

**5. Inline everything**  
Example of inline panel.  
You can use this same technique for panels, errmsgs, jcl skeletons, help panels, pop-ups all into the same routine which greatly simplifies code management by having all the necessary objects in one place.  

**6. Panels with embedded Rexx**  
How to use Rexx in a panel definition, in the INIT or PROC section etc.  
This can be useful to create much more powerful editting in the panel, and also to get some complexity into determining values to be displayed on the panel.

**7. File allocations**  
This example shows how to use BPXWDYN to dynamically add a PDS to a ddname.
Note that there are 'protected' ddnames used by ISPF that you won't be able to update, eg. STEPLIB.  

**8. Integrate into ISPF**  
This is a simple little routine to show how easy it is to integrate your Rexx routine with ISPF, in this example 3.4, to get the name of the dataset into your processing.

**9. ooRexx to drive Windows programs - 1**  
A sizeable example from IBM, shows connecting to DB2 and doing work to retrieve LOB values.  

**9. ooRexx to drive Windows programs - 2**  
Another large example, this one talks to lotus notes and updates excel.  
This is a cut-down version, a number of paragraphs have been removed so you might get some errors if you tried to run it :)  
Left a lot of code in place because the routine does a lot of things you might find useful  
- reads/updates lotus notes  
- uses oorexx classes as well as our own tailored internal class  
- reads/updates/saves excel spreadsheets  
- works with ms-word  
- writes to a plain text file  
- the classes need to be in the path

**10. Readlog**  
Routine to read OPERLOG b/w start dtime to end dtime reporting all lines or just those highlighted RED.  
The huge benefit of this is that the OPERLOG lines will be written to the held output where you can "SE" against  the file to use the edit commands to greatly simplify working with the information.  



### How do I get set up? ###
Simply upload or cut/paste the example text file to your own TSO library and they should run fine. All the mainframe examples have been tested. The ooRexx programs haven't been tested but they are taken from implemented code, so the syntax is correct.


### Contribution guidelines ###
These examples are all original code by Frank Pope unless attributed otherwise.  
Code by Frank Pope is freely available to use as and how you like. No express or implied guarantees or warranties are provided.  
See the attributed code for their own copyright requirements.  



* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)


